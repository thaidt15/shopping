import axiosClient from "contraints/APIUtils";

const UserAPI = {
    getCurrentUser: () => {
        const url = `/user/me`;
        return axiosClient.get(url);
      },
  
      
};

export default UserAPI;
