import axiosClient from "contraints/APIUtils";

const ForgotPasswordAPI = {
    sendResetCode: (email) => {
        const url = `/common/forgot-password?email=${email}`;
        return axiosClient.post(url);
      },
  
      resetPassword: (email, code, newPassword) => {
        const url = `/common/reset-password`;
        const data = { email, code, newPassword }; 
        return axiosClient.post(url, data);
      },
};

export default ForgotPasswordAPI;
