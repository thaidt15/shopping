import axiosClient from "contraints/APIUtils";
const LoginAPI = {
    Login: (email, password) => {
        const url = `/common/login`;
        const data = { email, password };
        return axiosClient.post(url, data);
    },
    register: (email, password, fullName) => {
        const url = `/common/register`;
        const data = { email, password , fullName};
        return axiosClient.post(url, data);
    },
};
export default LoginAPI;