import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
  return (
    <footer className="mt-4 p-3 bg-light">
      <Container>
        <Row>
          <Col>
            <p>Footer content goes here</p>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
