import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import LoginAPI from 'api/LoginAPI';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = ({ onLogin }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await LoginAPI.Login(email, password);
      if (response.data && response.data.accessToken) {
        localStorage.setItem('token', response.data.accessToken);
        if (response.data.role === 'ADMIN') {
          history.push('/admin/dashboard');
        } else {
          history.push('/recharge');
        }
        onLogin();
      } else {
        toast.error('Tài khoản hoặc mật khẩu bạn vừa nhập chưa đúng!');
      }
    } catch (error) {
     
      if (error.response && error.response.status === 401) {
        toast.error('Tài khoản hoặc mật khẩu bạn vừa nhập chưa đúng!');
      } else if (error.response && error.response.status === 403) {
        const responseData = error.response.data;
        if (responseData && responseData.message) {
          toast.error(responseData.message);
        } else {
          toast.error('Vui lòng xác nhận tài khoản của bạn trước khi đăng nhập.');
        }
      } else if (error.response && error.response.status === 423) {
        toast.error('Xin lỗi! Tài khoản của bạn đã bị chặn');
      } else {
        toast.error('Đã xảy ra lỗi khi đăng nhập. Vui lòng thử lại.');
      }
    }
  };

  return (
    <Container fluid className="login-container">
      <Row className="justify-content-center align-items-center h-100">
        <Col md={6}>
          <Card className="login-card">
            <Card.Body>
              <h2 className="text-center mb-4">ĐĂNG NHẬP</h2>
              <Form onSubmit={handleLogin}>
                <Form.Group controlId="formBasicEmail" className="mb-3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Vui lòng nhập địa chỉ email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword" className="mb-3">
                  <Form.Label>Mật khẩu</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Vui lòng nhập mật khẩu của bạn"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                <Button variant="primary" type="submit" block>
                  Đăng nhập
                </Button>
              </Form>
              <div className="mt-3 text-center">
                Bạn chưa có tài khoản?{' '}
                <Link to="/register">Đăng ký ngay</Link>
              </div>
              <div className="mt-3 text-center">
                Bạn quên mật khẩu của mình?{' '}
                <Link to="/forgot-password">Lấy lại mật khẩu</Link>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
    </Container>
  );
};

export default Login;
