import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card, Modal, Spinner } from 'react-bootstrap';
import LoginAPI from 'api/LoginAPI';
import { ToastContainer, toast } from 'react-toastify';
import { Link, useHistory } from 'react-router-dom';

const Registration = ({ onRegistration }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [fullName, setFullName] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    const [passwordValidation, setPasswordValidation] = useState({
        isValid: false,
        message: '',
    });

    const [passwordMatch, setPasswordMatch] = useState(false);

    const handlePasswordChange = (password) => {
        // Password validation criteria
        const regexUpperCase = /[A-Z]/;
        const regexSpecialChar = /[!@#$%^&*(),.?":{}|<>]/;

        // Check if password meets criteria
        const isValid =
            password.length >= 6 && regexUpperCase.test(password) && regexSpecialChar.test(password);

        // Set validation message
        const message = isValid
            ? ''
            : 'Mật khẩu cần ít nhất 6 ký tự, có ít nhất 1 chữ in hoa và 1 ký tự đặc biệt.';

        setPasswordValidation({ isValid, message });
    };
    const [isConfirmPasswordTouched, setConfirmPasswordTouched] = useState(false);
    const handleConfirmPasswordChange = (confirmPassword) => {
        // Check if passwords match only if the user has touched the confirm password field
        if (isConfirmPasswordTouched) {
            setPasswordMatch(confirmPassword === password);
        }
    };
    const handleConfirmPasswordBlur = () => {
        // Mark confirm password as touched when the user leaves the input field
        setConfirmPasswordTouched(true);
    };

    const handleRegistration = async (e) => {
        e.preventDefault();

        // Check if passwords match
        if (!passwordMatch) {
            toast.error('Mật khẩu và xác nhận mật khẩu không trùng nhau');
            return;
        }

        // Check if password is valid
        if (!passwordValidation.isValid) {
            toast.error(passwordValidation.message);
            return;
        }

        try {
            setLoading(true);
            const response = await LoginAPI.register(email, password, fullName);
            toast.success(response.data);

            setTimeout(() => {
                history.push(`/login`);
            }, 3000);
        } catch (error) {
            console.error(error);
            toast.error(error.response?.data || 'Đã xảy ra lỗi! Vui lòng thử lại sau');
        } finally {
            setLoading(false);
        }
    };

    return (
        <Container fluid className="login-container">
            <Row className="justify-content-center align-items-center h-100">
                <Col md={6}>
                    <Card className="login-card">
                        <Card.Body>
                            <h2 className="text-center mb-4">ĐĂNG KÝ</h2>
                            <Form onSubmit={handleRegistration}>
                                <Form.Group controlId="formBasicEmail" className="mb-3">
                                    <Form.Label>Họ và tên</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Vui lòng nhập tên của bạn"
                                        value={fullName}
                                        onChange={(e) => setFullName(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail" className="mb-3">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Vui lòng nhập địa chỉ email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword" className="mb-3">
                                    <Form.Label>Mật khẩu</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Vui lòng nhập mật khẩu của bạn"
                                        value={password}
                                        onChange={(e) => {
                                            setPassword(e.target.value);
                                            handlePasswordChange(e.target.value);
                                        }}
                                        required
                                    />
                                    {passwordValidation.message && (
                                        <Form.Text className="text-danger">
                                            {passwordValidation.message}
                                        </Form.Text>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="formConfirmPassword" className="mb-3">
                                    <Form.Label>Nhập lại mật khẩu</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Nhập lại mật khẩu"
                                        value={confirmPassword}
                                        onChange={(e) => {
                                            setConfirmPassword(e.target.value);
                                            handleConfirmPasswordChange(e.target.value);
                                        }}
                                        onBlur={handleConfirmPasswordBlur}
                                        required
                                    />
                                    {isConfirmPasswordTouched && (
                                        <React.Fragment>
                                            {passwordMatch ? (
                                                <Form.Text className="text-success">Mật khẩu trùng khớp</Form.Text>
                                            ) : (
                                                <Form.Text className="text-danger">Mật khẩu không trùng khớp</Form.Text>
                                            )}
                                        </React.Fragment>
                                    )}
                                </Form.Group>

                                <Button variant="primary" type="submit" block disabled={loading}>
                                    Đăng ký
                                </Button>
                            </Form>
                            <div className="mt-3 text-center">
                                Bạn đã có tài khoản? <Link to="/login">Đăng nhập ngay</Link>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Modal show={loading} centered backdrop="static" keyboard={false} aria-labelledby="loading-modal">
                <Modal.Body className="text-center">
                    <Spinner animation="border" />
                    <div className="mt-2">Đang xử lý...</div>
                </Modal.Body>
            </Modal>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </Container>
    );
};

export default Registration;
