import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card, Spinner, Modal } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import ForgotPasswordAPI from 'api/ForgotPasswordAPI';
import { ToastContainer, toast } from 'react-toastify';

const ForgotPasswordEmail = () => {
    const [email, setEmail] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            setLoading(true);
            setError(null);

            // Call your API to send the reset code
            const response = await ForgotPasswordAPI.sendResetCode(email);
            toast.success(response.data);

            setTimeout(() => {
                history.push(`/enter-code/${email}`);
            }, 3000);
        } catch (error) {
            const err = error.response.data ? error.response.data : "Đã xảy ra lỗi! Vui lòng thử lại sau"
            toast.error(err)
        } finally {
            setLoading(false);
        }
    };

    return (
        <Container fluid className="login-container">
            <Row className="justify-content-center align-items-center h-100">
                <Col md={6}>
                    <Card className="login-card">
                        <Card.Body>
                            <h2 className="text-center mb-4">Quên Mật Khẩu</h2>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group controlId="formBasicEmail" className="mb-3">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Nhập địa chỉ email của bạn"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Button variant="primary" type="submit" block disabled={loading}>
                                    {loading ? (
                                        <>
                                            <Spinner animation="border" size="sm" className="me-2" />
                                            Đang xử lý...
                                        </>
                                    ) : (
                                        'Gửi mã xác nhận'
                                    )}
                                </Button>
                            </Form>
                            <div className="mt-3 text-center">
                                <Link to="/login">Quay lại đăng nhập</Link>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Modal show={loading} centered backdrop="static" keyboard={false} aria-labelledby="loading-modal">
                <Modal.Body className="text-center">
                    <Spinner animation="border" />
                    <div className="mt-2">Đang xử lý...</div>
                </Modal.Body>
            </Modal>
            <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
        </Container>
    );
};

export default ForgotPasswordEmail;
