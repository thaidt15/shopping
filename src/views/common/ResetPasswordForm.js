import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';

const ResetPasswordForm = ({ onSubmit }) => {
  const [newPassword, setNewPassword] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Validate the password (add your validation logic if needed)
    onSubmit(newPassword);
  };

  return (
    <Container fluid className="login-container">
      <Row className="justify-content-center align-items-center h-100">
        <Col md={6}>
          <Card className="login-card">
            <Card.Body>
              <h2 className="text-center mb-4">Đặt Lại Mật Khẩu</h2>
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicNewPassword" className="mb-3">
                  <Form.Label>Mật Khẩu Mới</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Nhập mật khẩu mới"
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                <Button variant="primary" type="submit" block>
                  Đặt Lại Mật Khẩu
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default ResetPasswordForm;
