import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import ForgotPasswordAPI from 'api/ForgotPasswordAPI';
import { useParams, useHistory } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const ForgotPasswordCode = () => {
    const { email } = useParams();
    const [code, setCode] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [passwordValidation, setPasswordValidation] = useState({
        isValid: false,
        message: '',
    });
    const history = useHistory();

    const handlePasswordChange = (password) => {
        // Password validation criteria
        const regexUpperCase = /[A-Z]/;
        const regexSpecialChar = /[!@#$%^&*(),.?":{}|<>]/;

        // Check if password meets criteria
        const isValid =
            password.length >= 6 && regexUpperCase.test(password) && regexSpecialChar.test(password);

        // Set validation message
        const message = isValid
            ? ''
            : 'Mật khẩu cần ít nhất 6 ký tự, có ít nhất 1 chữ in hoa và 1 ký tự đặc biệt.';

        setPasswordValidation({ isValid, message });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        // Check if passwords match
        if (newPassword !== confirmPassword) {
            setPasswordValidation({
                isValid: false,
                message: 'Mật khẩu và mật khẩu xác nhận không khớp.',
            });
            return;
        }

        // Check if password is valid
        if (!passwordValidation.isValid) {
            setPasswordValidation({
                isValid: false,
                message: 'Mật khẩu không đáp ứng yêu cầu.',
            });
            return;
        }

        try {
            const response = await ForgotPasswordAPI.resetPassword(email, code, newPassword);
            toast.success(response.data)

            setTimeout(() => {
                history.push(`/login`);
            }, 3000);
        } catch (error) {
           
            setPasswordValidation({
                isValid: false,
                message: error.response?.data || 'Đã xảy ra lỗi! Vui lòng thử lại sau',
            });
        }
    };

    return (
        <Container fluid className="login-container">
            <Row className="justify-content-center align-items-center h-100">
                <Col md={6}>
                    <Card className="login-card">
                        <Card.Body>
                            <h2 className="text-center mb-4">Đặt Lại Mật Khẩu Của Bạn</h2>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group controlId="formBasicCode" className="mb-3">
                                    <Form.Label>Mã Xác Nhận</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập mã xác nhận từ email"
                                        value={code}
                                        onChange={(e) => setCode(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="formBasicNewPassword" className="mb-3">
                                    <Form.Label>Mật Khẩu Mới</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Nhập mật khẩu mới"
                                        value={newPassword}
                                        onChange={(e) => {
                                            setNewPassword(e.target.value);
                                            handlePasswordChange(e.target.value);
                                        }}
                                        required
                                    />
                                    {passwordValidation.message && (
                                        <Form.Text className="text-danger">
                                            {passwordValidation.message}
                                        </Form.Text>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="formBasicConfirmPassword" className="mb-3">
                                    <Form.Label>Nhập Lại Mật Khẩu</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Nhập lại mật khẩu mới"
                                        value={confirmPassword}
                                        onChange={(e) => setConfirmPassword(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Button variant="primary" type="submit" block>
                                    Đặt Lại Mật Khẩu
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
      
        </Container>
    );
};

export default ForgotPasswordCode;
