import React from 'react';
import { Container, Row, Col, Carousel, Card, Button } from 'react-bootstrap';
import Header from '../../components/user/Header';
import Footer from '../../components/user/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';

const HomePage = () => {
  // Sample data for the slideshow
  const slideshowImages = [
    'https://via.placeholder.com/1200x400',
    'https://via.placeholder.com/1200x400',
    'https://via.placeholder.com/1200x400',
  ];

  // Sample data for the category section
  const categories = [
    { name: 'Category 1', quantity: 10, sold: 5, imageUrl: 'https://via.placeholder.com/150' },
    { name: 'Category 2', quantity: 15, sold: 8, imageUrl: 'https://via.placeholder.com/150' },
    { name: 'Category 3', quantity: 20, sold: 12, imageUrl: 'https://via.placeholder.com/150' },
  ];

  return (
    <div>
      <Header />

      <Container fluid>
        <Row className="mt-4">
          <Col>
            <Carousel>
              {slideshowImages.map((image, index) => (
                <Carousel.Item key={index}>
                  <img className="d-block w-100" src={image} alt={`Slide ${index}`} />
                </Carousel.Item>
              ))}
            </Carousel>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col>
            <h2>Danh Mục Tài khoản</h2>
            <Row>
              {categories.map((category, index) => (
                <Col key={index} md={4} className="mb-4">
                  <Card>
                    <Card.Img variant="top" src={category.imageUrl} />
                    <Card.Body>
                      <Card.Title>{category.name}</Card.Title>
                      <Card.Text>
                        <strong>Số lượng:</strong> {category.quantity}
                        <br />
                        <strong>Đã bán:</strong> {category.sold}
                      </Card.Text>
                      <Button variant="primary">Xem Thêm</Button>
                    </Card.Body>
                  </Card>
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>

      <Footer />
    </div>
  );
};

export default HomePage;
