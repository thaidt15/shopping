import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Alert } from 'react-bootstrap';
import axios from 'axios';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import UserAPI from 'api/UserAPI';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const TransactionPage = () => {
  const [amount, setAmount] = useState('');
  const [qrCode, setQrCode] = useState('');
  const [error, setError] = useState(null);
  const [remainingTime, setRemainingTime] = useState(300); // 5 minutes in seconds
  const [userId, setUserId] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    // Fetch current user ID
    const fetchCurrentUser = async () => {
      try {
        const response = await UserAPI.getCurrentUser();
        setUserId(response.data.id);
        setUser(response.data)
      } catch (error) {
      }
    };

    fetchCurrentUser();
  }, []);

  const handleNapClick = async () => {
    try {
      const response = await axios.get(`https://vietqr.co/api/generate/acb/12690291/VIETQR.CO/${amount}/${userId}?isMask=0`, {
        responseType: 'arraybuffer',
      });

      const base64Image = btoa(new Uint8Array(response.data).reduce((data, byte) => data + String.fromCharCode(byte), ''));
      setQrCode(`data:image/png;base64,${base64Image}`);
      setError(null);
      setRemainingTime(300); // Reset the countdown timer after each successful click
    } catch (error) {
      setError('Đã xảy ra lỗi. Vui lòng thử lại.');
    }
  };
  const [showToast, setShowToast] = useState(false);
  useEffect(() => {
    // const socket = new SockJS('http://scms.mom:8080/socket');
    const socket = new SockJS('http://localhost:8080/socket');
    const stompClient = Stomp.over(socket);
// console.log("socket" +JSON.stringify(stompClient))

    stompClient.connect({}, () => {
      // toast("Test notification");
     stompClient.subscribe('/queue/notifications', (message) => {
      console.log("Received message", message);
        // Trigger the toast notification directly
        toast.success("Your account balance has been updated.", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
        });
      });
    });

    let interval;
    if (remainingTime > 0) {
      interval = setInterval(() => {
        setRemainingTime((prevTime) => prevTime - 1);
      }, 1000);
    }

    return () => {
      clearInterval(interval);
      stompClient.disconnect();
    };
  }, [remainingTime]);



  const formatTime = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${String(minutes).padStart(2, '0')}:${String(remainingSeconds).padStart(2, '0')}`;
  };

  return (
    <Container fluid className="mt-4">
      <Row>
        <Col md={{ span: 4, offset: 4 }}>
          <h2 className="text-center mb-4">Nạp Tiền</h2>
          {error && <Alert variant="danger">{error}</Alert>}
          <div>
            {user && user.fullName ? (
              <p>
                Hello: <strong>{user.fullName}</strong>
              </p>
            ) : (
              <p>Địt mẹ mày đéo login mất tiền ráng chịu</p>
            )}
            
          </div>
          <div>
            {user && user.fullName ? (
              <p>
                Your balance: <strong>{user.accountBalance} VND</strong>
              </p>
            ) : (
              <p>Địt mẹ mày đéo login mất tiền ráng chịu</p>
            )}
            
          </div>
          <Form>
            <Form.Group controlId="formAmount">
              <Form.Label>Số Tiền</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập số tiền"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" onClick={handleNapClick} block>
              Nạp
            </Button>
          </Form>

          {qrCode && (
            <div className="mt-4 text-center">
              <h4>QR Code</h4>
              <h5>Thời gian hiệu lực còn: <strong>{formatTime(remainingTime)}</strong> </h5>
              <img src={qrCode} style={{ maxHeight: 400 }} alt="QR Code" className="img-fluid" />
            </div>
          )}
        </Col>
      </Row>
      <ToastContainer />
    </Container>
  );
};

export default TransactionPage;
