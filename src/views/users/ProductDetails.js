import React from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';

const ProductDetailsPage = () => {
  // Fixed data for the product
  const product = {
    id: '001',
    category: '100% Nick Đúng Mật Khẩu',
    price: '100,000 VND',
    accountType: 'Loại tài khoản',
    rank: 'Rank',
    champion: 'Tướng',
    skin: 'Trang phục',
    images: [
      'url_to_image_1',
      'url_to_image_2',
      'url_to_image_3',
      // Add more image URLs as needed
    ],
  };

  return (
    <Container fluid className="mt-4">
      <Row>
        <Col md={6}>
          <Card>
            <Card.Img variant="top" src={product.images[0]} />
            <Card.Body>
              <Card.Title>{product.category}</Card.Title>
              <Card.Text>
                <strong>Mã số:</strong> {product.id}
                <br />
                <strong>Loại tài khoản:</strong> {product.accountType}
                <br />
                <strong>Rank:</strong> {product.rank}
                <br />
                <strong>Tướng:</strong> {product.champion}
                <br />
                <strong>Trang phục:</strong> {product.skin}
                <br />
                <strong>Giá:</strong> {product.price}
              </Card.Text>
              <Button variant="primary" block>
                Mua Ngay
              </Button>
            </Card.Body>
          </Card>
        </Col>
        <Col md={1} className="d-flex align-items-center justify-content-center">
          <i className="icon-dot"></i>
        </Col>
        <Col md={5}>
          <div>
            {product.images.map((imageUrl, index) => (
              <img key={index} src={imageUrl} alt={`Product ${index + 1}`} className="img-fluid mb-3" />
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default ProductDetailsPage;
