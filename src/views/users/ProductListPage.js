import React from 'react';
import { Container, Row, Col, Alert, Button, Card, Form } from 'react-bootstrap';
import Header from '../../components/user/Header';
import Footer from '../../components/user/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';

const ProductListPage = () => {
  const productList = [
    {
      id: '001',
      category: 'Category 1',
      type: 'Type 1',
      rank: 'Rank 1',
      champion: 'Champion 1',
      price: '100',
      imageUrl: 'https://via.placeholder.com/150',
    },
    {
      id: '002',
      category: 'Category 2',
      type: 'Type 2',
      rank: 'Rank 2',
      champion: 'Champion 2',
      price: '150.000',
      imageUrl: 'https://via.placeholder.com/150',
    },
    // Add more product entries as needed
  ];

  // Sample data for filter dropdowns
  const filterOptions = {
    price: ['100', '150', '200'], // Sample price options
    type: ['Type 1', 'Type 2', 'Type 3'], // Sample type options
    rank: ['Rank 1', 'Rank 2', 'Rank 3'], // Sample rank options
  };

  return (
    <div>
      <Header />

      <Container fluid>
        <Row className="mt-4">
          <Col>
            <Alert variant="info" style={{marginTop: 90}}>
              <strong>Danh Mục ...</strong>
              <br />
              100% Nick Đúng Mật Khẩu
              <br />
              100% Trắng Thông Tin
              <br />
              TẤT CẢ CÁC TÀI KHOẢN "Category" ĐANG ĐƯỢC GIẢM GIÁ 50%
              <br />
              AE MUA CHƠI NGAY NÀO !!! [ Hỗ Trợ Lỗi Liên Hệ Page Nhé ].
            </Alert>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col>
            <Form>
              <Row className="mb-3">
                <Col md={2}>
                  <Form.Control type="text" placeholder="Nhập mã số" />
                </Col>
                <Col md={2}>
                  <Form.Control type="text" placeholder="Nhập giá tiền" />
                </Col>
                <Col md={2}>
                  <Form.Control as="select">
                    <option value="type">Loại Tài Khoản</option>
                    {filterOptions.type.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </Form.Control>
                </Col>
                <Col md={2}>
                  <Form.Control as="select">
                    <option value="rank">Loại Rank</option>
                    {filterOptions.rank.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </Form.Control>
                </Col>
                <Col md={2}>
                  <Button variant="primary" type="submit">
                    Tìm Kiếm
                  </Button>
                </Col>
                <Col md={2}>
                  <Button variant="secondary" type="submit">
                    Tất Cả
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>

        <Row>
          {productList.map((product) => (
            <Col key={product.id} md={3} className="mb-4">
            <Card>
              <div className="position-relative">
                <Card.Img variant="top" src={product.imageUrl} />
                <span className="text-red font-weight-bold" style={{ position: 'absolute', top: 5, left: 5 }}>
                  Mã Số: {product.id}
                </span>
                <span className="text-white" style={{ position: 'absolute', bottom: 5, left: 5 }}>
                  Ních ngon giá rẻ
                </span>
              </div>
              <Card.Body>
                <Card.Title>{product.category}</Card.Title>
                <Card.Text>
                  <strong>Rank:</strong> {product.rank}
                  <br />
                  <strong>Tướng:</strong> {product.champion}
                </Card.Text>
                <Button variant="light">{`Giá: ${product.price}`}</Button>
                <Button variant="primary" className="float-right">
                  Chi Tiết
                </Button>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">{`Mã Số: ${product.id}`}</small>
              </Card.Footer>
            </Card>
          </Col>
          ))}
        </Row>
      </Container>

      <Footer />
    </div>
  );
};

export default ProductListPage;
