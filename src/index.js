import React from "react";
import ReactDOM from "react-dom/client";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/scss/light-bootstrap-dashboard-react.scss?v=2.0.0";
import "./assets/css/demo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import Login from "views/common/Login";
import AdminLayout from "layouts/Admin.js";
import Registration from "views/common/Registration";
import ForgotPasswordCode from "views/common/EnterCodeForm";
import ForgotPasswordEmail from "views/common/EnterEmailForm";
import ResetPasswordForm from "views/common/ResetPasswordForm";
import HomePage from "views/users/Home";
import ProductListPage from "views/users/ProductListPage";
import ProductDetailsPage from "views/users/ProductDetails";
import Recharge from "views/users/Recharge";
const App = () => {
  const [authenticated, setAuthenticated] = useState(false);

  const handleLogin = () => {
    setAuthenticated(true);
  };
  const handleRegistration = () => {
    setAuthenticated(true);
  };

  return (
    <BrowserRouter>
    <Switch>
      <Route path="/login" render={(props) => <Login onLogin={handleLogin} />} />
      <Route path="/register" render={(props) => <Registration onRegistration={handleRegistration} />} />
      <Route path="/forgot-password" component={ForgotPasswordEmail} />
      <Route path="/enter-code/:email" component={ForgotPasswordCode} />
      <Route path="/reset-password" component={ResetPasswordForm} />
      <Route path="/home" component={HomePage} />
      {/* <Route path="/product-list" component={ProductListPage} />
      <Route path="/product-details" component={ProductDetailsPage}/> */}
      <Route path="/recharge" component={Recharge}/>
      <Route
        path="/admin"
        render={(props) =>
          authenticated ? <AdminLayout {...props} /> : <Redirect to="/login" />
        }
      />
      <Redirect exact from="/" to="/login" />
    </Switch>
  </BrowserRouter>
  );
};

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);