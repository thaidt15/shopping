
import Dashboard from "views/Dashboard.js";
import ProductManagement from "views/ProductManagement";
import TableList from "views/TableList.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-35",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/product-manage",
    name: "Quản Lý Sản Phẩm",
    icon: "nc-icon nc-circle-09",
    component: ProductManagement,
    layout: "/admin"
  },
  {
    path: "/user-manage",
    name: "Quản Lý Người Dùng",
    icon: "nc-icon nc-notes",
    component: TableList,
    layout: "/admin"
  }
];

export default dashboardRoutes;
